#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "Task.h"
#include <Ticker.h>

#define MAX_TASKS 50

class Scheduler {
  
  int basePeriod;
  int nTasks;
  Ticker ticker;
  Task* taskList[MAX_TASKS]; 
  long lastTick; 

public:
  void init(int basePeriod);  
  virtual bool addTask(Task* task);
  virtual void schedule();
};

#endif
