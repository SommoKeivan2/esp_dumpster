#ifndef __UINFOT__
#define __UINFOT__

#include "Common.h"
#include "ReadPotTask.h"
#include <ArduinoJson.h>

class UpdateInfoTask : public Task 
{

public:

	UpdateInfoTask(String address, Led* ledAvail, Led* ledNotAvail, ReadPotTask* readPotTask);
	void tick();

private:
	String address;
	HTTPClient http;
	Led* ledAvail;
	Led* ledNotAvail;
    ReadPotTask* readPotTask;
    bool isForced;
};

#endif
