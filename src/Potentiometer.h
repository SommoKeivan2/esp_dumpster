#ifndef __pot__
#define __pot__

#include "Common.h"

class Potentiometer {

public:
	Potentiometer(int potPin);
	bool isPotChange();

private:
	int potPin;
};

#endif
