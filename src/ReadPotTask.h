#ifndef __READPOT__
#define __READPOT__

#include "Task.h"
#include "Potentiometer.h"
#include "Led.h"
#include <ESP8266HTTPClient.h>

class ReadPotTask : public Task 
{

public:

	ReadPotTask(Potentiometer* pot, String sendWeightAddress, String sendAvailableInfoAddress, Led* ledAvail, Led* ledNotAvail);
	void tick();
	void verifyAvailable();

private:
	Potentiometer* pot;
	String sendWeightAddress;
	String sendAvailableInfoAddress;
	int lastPotVal;
	int actualWeight;
	HTTPClient http;
	Led* ledAvail;
	Led* ledNotAvail;
	void sendIsAvailableInfo(int isAvailable);
};

#endif
