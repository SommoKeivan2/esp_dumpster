#include "ReadPotTask.h"

ReadPotTask::ReadPotTask(Potentiometer *pot, String sendWeightAddress, String sendAvailableInfoAddress, Led *ledAvail, Led *ledNotAvail)
{
    this->pot = pot;
    this->lastPotVal = -1;
    this->sendAvailableInfoAddress = sendAvailableInfoAddress;
    this->sendWeightAddress = sendWeightAddress;
    this->ledAvail = ledAvail;
    this->ledNotAvail = ledNotAvail;
    this->actualWeight = -1;
}

void ReadPotTask::tick()
{
    if (!pot->isPotChange() && potVal != this->lastPotVal)
    {
        this->actualWeight = map(potVal, 0, 1024, 0, wMax);
        Serial.println(actualWeight);
        if(lastPotVal > -1){
        String msg = "?weight=" + String(actualWeight);
        http.begin(this->sendWeightAddress + msg);
        int httpCode = http.GET();
        http.end(); //Close connection
        }
        lastPotVal = potVal;
    }
}

void ReadPotTask::verifyAvailable(){
    if (actualWeight >= wMax)
        {
            ledNotAvail->switchOn();
            ledAvail->switchOff();
            if (actualWeight >= wMax)
            {
                sendIsAvailableInfo(1);
            }
        } else {
            ledNotAvail->switchOff();
            ledAvail->switchOn();
            if (actualWeight < wMax)
            {
                sendIsAvailableInfo(0);
            }
        }
}

void ReadPotTask::sendIsAvailableInfo(int isAvailable)
{
    String msg = "?isAvailable=" + String(isAvailable);
    http.begin(this->sendAvailableInfoAddress + msg);
    int httpCode = http.GET();
    http.end();
}