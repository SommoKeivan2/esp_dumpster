#include "Scheduler.h"
#include <Arduino.h>
#include "Common.h"
#include "Potentiometer.h"
#include "Led.h"
#include "ReadPotTask.h"
#include "UpdateInfoTask.h"
#include <ESP8266WiFi.h>


//-------------------PINS----------------------
#define LED_AVAIL 5			//D1
#define LED_NOT_AVAIL 4		//D2
#define POT_PIN A0

//------------------PERIODS-------------------
#define SCHED_BASE_PERIOD 200
#define READ_POT_PERIOD 200
#define UPDATE_INFO_PERIOD 1000


//----------------GLOBALS----------------------------

volatile int potVal;
volatile int wMax;

//-----------LOCAL VARIABLES---------------------------

const char* ssid = "Home&Life SuperWiFi-E0E5";
const char* password = "YKXD8LPLMCKURP4X";
const char* path = "http://225a5c1b.ngrok.io/smart_dumpster/dumpster_service/";
const char* getInfoEsp = "getInfoEsp.php";
const char* sendWeightEsp = "sendWeightEsp.php";
const char* sendAvailableInfoEsp = "sendAvailableInfoEsp.php";

HTTPClient http;
bool firstTime;
bool isNotUsable;
int lastPotVal;
int actualWeight;
String address;
String msg;
Potentiometer* pot;
Led* ledAvail;
Led* ledNotAvail;
Task* updateTask;
ReadPotTask* readPotTask;
Scheduler sched;

//address = String(path) + String(sendWeightEsp);

void sendIsAvailableInfo(int isAvailable){
	address = String(path) + String(sendAvailableInfoEsp);
	msg = "?isAvailable=" + String(isAvailable);
	http.begin(address + msg);
	int httpCode = http.GET();
	http.end();
}

void setup(){
	Serial.begin(9600);
	ledAvail = new Led(LED_AVAIL);
	ledNotAvail = new Led(LED_NOT_AVAIL);
	pot = new Potentiometer(POT_PIN);
	readPotTask = new ReadPotTask(pot, String(path) + String(sendWeightEsp), String(path) + String(sendAvailableInfoEsp), ledAvail, ledNotAvail);
	updateTask = new UpdateInfoTask(String(path) + String(getInfoEsp), ledAvail, ledNotAvail, readPotTask);

	WiFi.begin(ssid, password);
	Serial.print("Connecting...");
	while (WiFi.status() != WL_CONNECTED) {  
		delay(500);
		Serial.print(".");
	}
	Serial.println(" Connected");

	readPotTask->init(READ_POT_PERIOD);
	updateTask->init(UPDATE_INFO_PERIOD);
	readPotTask->setActive(true);
	updateTask->setActive(true);

	sched.init(SCHED_BASE_PERIOD);
	sched.addTask(readPotTask);
	sched.addTask(updateTask);
}

void loop(){
	sched.schedule();
}
