#include "Potentiometer.h"
#include "Arduino.h"


Potentiometer::Potentiometer(int potPin){
    this->potPin = potPin;
    pinMode(this->potPin, INPUT);
    potVal =  analogRead(this->potPin);
}

bool Potentiometer::isPotChange(){
    int val = analogRead(this->potPin);
    if(val != potVal){
        potVal = val;
        return true;
    }
    return false;
}