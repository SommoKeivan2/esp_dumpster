#include "Scheduler.h"
#include "Arduino.h"

volatile bool timerFlag;

void Scheduler::init(int basePeriod){
  this->basePeriod = basePeriod;
  timerFlag = false;
  this->lastTick = millis();
  this->nTasks = 0;
}

bool Scheduler::addTask(Task* task){
  if (nTasks < MAX_TASKS-1){
    taskList[this->nTasks] = task;
    this->nTasks++;
    return true;
  } else {
    return false; 
  }
}
  
void Scheduler::schedule(){ 
  while (millis() - this->lastTick < this->basePeriod){}
  this->lastTick = millis();
  timerFlag = false;
  for (int i = 0; i < nTasks; i++){
    if (taskList[i]->isActive() && taskList[i]->updateAndCheckTime(basePeriod)){
      //Serial.println(String(i));
      taskList[i]->tick();
    }
  }
}
