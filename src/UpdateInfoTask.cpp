#include "UpdateInfoTask.h"

UpdateInfoTask::UpdateInfoTask(String address, Led* ledAvail, Led* ledNotAvail, ReadPotTask* readPotTask)
{
    this->address = address;
    this->ledAvail = ledAvail;
    this->ledNotAvail = ledNotAvail;
    this->readPotTask = readPotTask;
    http.begin(address);
    int httpCode = http.GET();
    if (httpCode > 0)
    {                                      //Check the returning code
        String payload = http.getString(); //Get the request response payload
        Serial.println(payload);           //Print the response payload
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, payload);
        // Test if parsing succeeds.
        if (error)
        {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
            return;
        }
        wMax = doc["wMax"];
        isForced = doc["isForced"];
    }
}

void UpdateInfoTask::tick()
{
    http.begin(address);
    int httpCode = http.GET();
    if (httpCode > 0)
    {                                      //Check the returning code
        String payload = http.getString(); //Get the request response payload
        Serial.println(payload);           //Print the response payload
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, payload);
        // Test if parsing succeeds.
        if (error)
        {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
            return;
        }
        wMax = doc["wMax"];
        if (isForced != doc["isForced"])
        {
            isForced = doc["isForced"];
            if (isForced)
            {
                this->readPotTask->setActive(false);
                this->ledAvail->switchOff();
                this->ledNotAvail->switchOn();
            } else
            {
                this->readPotTask->verifyAvailable();
                this->readPotTask->setActive(true);
            }
            
        }
    }
    http.end(); //Close connection
}